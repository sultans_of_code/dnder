from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm
from django.core.exceptions import ValidationError
from django.db import transaction

from .models import PC, DM, Session, Report, Session_PC


class UserRegistrationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = PC
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', 'is_dm')

    @transaction.atomic
    def save(self):
        user = super().save()
        if user.is_dm:
            dm = DM.objects.create(user_id=user)
        return user

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if PC.objects.filter(email=email).exists():
            raise ValidationError("Email already in use")
        return

    def is_valid(self):
        return super().is_valid()


class CustomUserChangeForm(forms.ModelForm):
    class Meta:
        model = PC
        fields = ('username', 'description', 'country', 'dark_theme')


class CustomAuthForm(AuthenticationForm):

    def confirm_login_allowed(self, user):
        if not user.email_confirmed:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )


class SessionForm(forms.ModelForm):
    class Meta:
        model = Session
        fields = ('name', 'country', 'city', 'address', 'date', 'time', 'language', 'description', 'private_session',
                  'min_number_of_players', 'max_number_of_players', 'current_number_of_players', 'fee',
                  'estimated_duration', 'edition', 'campaign', 'homebrew', 'starting_pc_level')


class UserReportForm(forms.ModelForm):
    class Meta:
        model = Report
        fields = ('report_title', 'report_text')
        report_title = forms.CharField()
        report_text = forms.Textarea()

    def save(self, commit=True):
        report = super().save(commit=False)
        if commit:
            report.save()
        return report


class ApplyToSessionForm(forms.ModelForm):
    class Meta:
        model = Session_PC
        fields = ('character_type', 'character_class', 'character_race', 'session_status')
        read_only = ('Session_id', 'PC_id')