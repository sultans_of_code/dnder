from django.contrib.auth.models import AnonymousUser


def theme_selector(request):
    if isinstance(request.user, AnonymousUser):
        return {'theme': False}
    return {'theme': request.user.dark_theme}
