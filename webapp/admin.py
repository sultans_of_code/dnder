from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from .models import *


class PCAdmin(admin.ModelAdmin):
    class Meta:
        model = PC

    list_display = ('username', 'email', 'status', 'is_dm')

    exclude = ['objects', 'groups', 'user_permissions']

    list_filter = (
        # ('is_staff', admin.BooleanFieldListFilter),
        ('is_active', admin.BooleanFieldListFilter),
        ('is_dm', admin.BooleanFieldListFilter),
        ('status', admin.AllValuesFieldListFilter),
    )


class SessionAdmin(admin.ModelAdmin):
    list_display = ('name', 'country', 'city', 'time', 'Session_id',)


class ReportAdmin(admin.ModelAdmin):
    list_display = ('reported_user_id', 'report_title', 'timestamp')


# Register your models here.
admin.site.register(PC, PCAdmin)
admin.site.register(DM, )
admin.site.register(Session, SessionAdmin)
admin.site.register(Report, ReportAdmin)


class WebappAdmin(admin.ModelAdmin):
    list_display = ""
