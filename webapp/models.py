from django.db import models
from django.core import validators
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django_countries.fields import CountryField


def user_img_path(instance, filename):
    return 'user_{0}/{1}'.format(instance.pk, filename)


def session_media_path(instance, filename):
    return 'session_{0}/{1}'.format(instance.session.Session_id, filename)


def calc_rating(*args):
    ret = 0
    num = 0
    for i, arg in enumerate(args, 1):
        ret += i * arg
        num += arg
    if num == 0:
        return 0
    return ret / num


class PC(AbstractUser):
    """
    """
    # Start constants
    NOVICE, BEGINNER, INTERMEDIATE, PROFESSIONAL, EXPERT, MASTER, GRANDMASTER = list(range(1, 8))
    PC_STATUS_CHOICES = [
        (NOVICE, "Novice"),
        (BEGINNER, "Beginner"),
        (INTERMEDIATE, "Intermediate"),
        (PROFESSIONAL, "Professional"),
        (EXPERT, "Expert"),
        (MASTER, "Master"),
        (GRANDMASTER, "Grandmaster"),
    ]
    # End constants

    email_confirmed = models.BooleanField('Email confirm', default=False)
    dark_theme = models.BooleanField("Uses Dark Theme", default=False)

    is_dm = models.BooleanField('DM status', default=False, help_text="Check this if you want to create sessions")

    date_of_birth = models.DateField(null=True, blank=True)
    country = CountryField(blank=True)

    number_of_sessions_played = models.PositiveIntegerField(default=0)
    picture = models.ImageField(upload_to=user_img_path, blank=True)
    description = models.TextField(blank=True)
    status = models.IntegerField(choices=PC_STATUS_CHOICES, default=NOVICE)
    rating_1 = models.PositiveIntegerField(default=0)
    rating_2 = models.PositiveIntegerField(default=0)
    rating_3 = models.PositiveIntegerField(default=0)
    rating_4 = models.PositiveIntegerField(default=0)
    rating_5 = models.PositiveIntegerField(default=0)

    ban_expiration_date = models.DateField(null=True, blank=True)

    REQUIRED_FIELDS = ['email', 'first_name', 'last_name', 'country']

    def __str__(self):
        return self.username

    def delete(self, using=None, keep_parents=False):
        if self.picture is not None:
            self.picture.storage.delete(self.picture.name)
        super().delete()

    class Meta:
        verbose_name = 'PC'
        verbose_name_plural = 'PCs'

    def get_rating(self):
        return calc_rating(self.rating_1, self.rating_2, self.rating_3, self.rating_4, self.rating_5)


class DM(models.Model):
    user_id = models.OneToOneField(
        'webapp.PC',
        on_delete=models.CASCADE,
        primary_key=True,
    )
    rating_1 = models.PositiveIntegerField(default=0)
    rating_2 = models.PositiveIntegerField(default=0)
    rating_3 = models.PositiveIntegerField(default=0)
    rating_4 = models.PositiveIntegerField(default=0)
    rating_5 = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.user_id.username

    def get_rating(self):
        return calc_rating(self.rating_1, self.rating_2, self.rating_3, self.rating_4, self.rating_5)


class Session(models.Model):
    Session_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    country = CountryField()
    city = models.CharField(max_length=30)
    address = models.CharField(max_length=50)
    date = models.DateField()
    time = models.TimeField()
    language = models.CharField(max_length=20)
    description = models.TextField(blank=True)
    review_text = models.TextField(blank=True)

    private_session = models.BooleanField()
    min_number_of_players = models.PositiveIntegerField(validators=[validators.MinValueValidator(1)])
    max_number_of_players = models.PositiveIntegerField()
    current_number_of_players = models.PositiveIntegerField()

    fee = models.DecimalField(max_digits=3, decimal_places=2)
    estimated_duration = models.DurationField()
    edition = models.CharField(max_length=100)
    campaign = models.CharField(max_length=100)
    homebrew = models.BooleanField()
    starting_pc_level = models.IntegerField(validators=[
        validators.MinValueValidator(1),
        validators.MaxValueValidator(20),
    ])

    attendees = models.ManyToManyField(PC, through='Session_PC')
    host_DMs = models.ManyToManyField(DM, through='Session_DM')

    def __str__(self):
        return self.name


class Session_PC(models.Model):
    PC_id = models.ForeignKey(
        'webapp.PC',
        on_delete=models.CASCADE,
    )
    Session_id = models.ForeignKey(
        'webapp.Session',
        on_delete=models.CASCADE,
    )
    GOING = 'GO'
    PENDING = 'PE'
    INTERESTED = 'IN'
    SESSION_STATUS_CHOICES = [
        (GOING, 'Going'),
        (PENDING, 'Pending approval'),
        (INTERESTED, 'Interested'),
    ]
    session_status = models.CharField(
        max_length=2,
        choices=SESSION_STATUS_CHOICES,
        blank=False
    )
    character_type = models.CharField(max_length=50, blank=True)
    character_class = models.CharField(max_length=50, blank=True)
    character_race = models.CharField(max_length=50, blank=True)


class Session_DM(models.Model):
    DM_id = models.ForeignKey(
        'webapp.DM',
        on_delete=models.CASCADE,
    )
    Session_id = models.ForeignKey(
        'webapp.Session',
        on_delete=models.CASCADE,
    )


class SessionFile(models.Model):
    session = models.ForeignKey(
        'webapp.Session',
        on_delete=models.CASCADE,
    )
    file = models.FileField(upload_to=session_media_path)
    date_upload = models.DateField(auto_now_add=True)

    def delete(self, using=None, keep_parents=False):
        self.file.storage.delete(self.file.name)
        super().delete()


class Report(models.Model):
    report_id = models.AutoField(primary_key=True)

    reported_user_id = models.ForeignKey(
        PC,
        on_delete=models.CASCADE,
        related_name='reported',
    )

    caller_user_id = models.ForeignKey(
        PC,
        on_delete=models.CASCADE,
        related_name='caller',
    )

    report_title = models.CharField(max_length=100)
    report_text = models.TextField(max_length=690)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.reported_user_id.username
