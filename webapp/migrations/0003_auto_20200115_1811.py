# Generated by Django 2.2.6 on 2020-01-15 18:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0002_auto_20200114_1102'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pc',
            name='is_dm',
            field=models.BooleanField(default=False, help_text='Check this if you want to create sessions', verbose_name='DM status'),
        ),
    ]
