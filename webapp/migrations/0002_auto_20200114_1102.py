# Generated by Django 2.2.6 on 2020-01-14 11:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='pc',
            options={'verbose_name': 'PC', 'verbose_name_plural': 'PCs'},
        ),
        migrations.AddField(
            model_name='pc',
            name='email_confirmed',
            field=models.BooleanField(default=False, verbose_name='Email confirm'),
        ),
    ]
