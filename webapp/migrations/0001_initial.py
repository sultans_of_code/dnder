# Generated by Django 2.2.6 on 2020-01-08 22:05

from django.conf import settings
import django.contrib.auth.models
import django.contrib.auth.validators
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import django_countries.fields
import webapp.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0011_update_proxy_permissions'),
    ]

    operations = [
        migrations.CreateModel(
            name='PC',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()], verbose_name='username')),
                ('first_name', models.CharField(blank=True, max_length=30, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=150, verbose_name='last name')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('is_dm', models.BooleanField(default=False, verbose_name='DM status')),
                ('date_of_birth', models.DateField(blank=True, null=True)),
                ('country', django_countries.fields.CountryField(blank=True, max_length=2)),
                ('number_of_sessions_played', models.PositiveIntegerField(default=0)),
                ('picture', models.ImageField(blank=True, upload_to=webapp.models.user_img_path)),
                ('description', models.TextField(blank=True)),
                ('status', models.IntegerField(choices=[(1, 'Novice'), (2, 'Beginner'), (3, 'Intermediate'), (4, 'Professional'), (5, 'Expert'), (6, 'Master'), (7, 'Grandmaster')], default=1)),
                ('rating_1', models.PositiveIntegerField(default=0)),
                ('rating_2', models.PositiveIntegerField(default=0)),
                ('rating_3', models.PositiveIntegerField(default=0)),
                ('rating_4', models.PositiveIntegerField(default=0)),
                ('rating_5', models.PositiveIntegerField(default=0)),
                ('ban_expiration_date', models.DateField(blank=True, null=True)),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'abstract': False,
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Session',
            fields=[
                ('Session_id', models.IntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=50)),
                ('country', django_countries.fields.CountryField(max_length=2)),
                ('city', models.CharField(max_length=30)),
                ('address', models.CharField(max_length=50)),
                ('time', models.TimeField()),
                ('language', models.CharField(max_length=20)),
                ('description', models.TextField()),
                ('review_text', models.TextField()),
                ('private_session', models.BooleanField()),
                ('min_number_of_players', models.IntegerField(validators=[django.core.validators.MinValueValidator(1)])),
                ('max_number_of_players', models.IntegerField()),
                ('current_number_of_players', models.IntegerField()),
                ('fee', models.DecimalField(decimal_places=2, max_digits=3)),
                ('estimated_duration', models.DurationField()),
                ('edition', models.CharField(max_length=100)),
                ('campaign', models.CharField(max_length=100)),
                ('homebrew', models.BooleanField()),
                ('starting_pc_level', models.IntegerField(validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(20)])),
            ],
        ),
        migrations.CreateModel(
            name='DM',
            fields=[
                ('user_id', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('rating_1', models.PositiveIntegerField(default=0)),
                ('rating_2', models.PositiveIntegerField(default=0)),
                ('rating_3', models.PositiveIntegerField(default=0)),
                ('rating_4', models.PositiveIntegerField(default=0)),
                ('rating_5', models.PositiveIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='SessionFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file', models.FileField(upload_to=webapp.models.session_media_path)),
                ('date_upload', models.DateField(auto_now_add=True)),
                ('session', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='webapp.Session')),
            ],
        ),
        migrations.CreateModel(
            name='Session_PC',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('session_status', models.CharField(choices=[('GO', 'Going'), ('PE', 'Pending approval'), ('IN', 'Interested')], max_length=2)),
                ('PC_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('Session_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='webapp.Session')),
            ],
        ),
        migrations.AddField(
            model_name='session',
            name='attendees',
            field=models.ManyToManyField(through='webapp.Session_PC', to=settings.AUTH_USER_MODEL),
        ),
        migrations.CreateModel(
            name='Session_DM',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Session_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='webapp.Session')),
                ('DM_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='webapp.DM')),
            ],
        ),
        migrations.AddField(
            model_name='session',
            name='host_DMs',
            field=models.ManyToManyField(through='webapp.Session_DM', to='webapp.DM'),
        ),
    ]
