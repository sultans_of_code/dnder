import datetime

from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.utils import datetime_safe
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views.generic import ListView
from django.views.generic.edit import CreateView

from webapp.models import PC, Session, Report
from webapp.tokens import account_activation_token
from .forms import SessionForm, ApplyToSessionForm
from .forms import UserRegistrationForm, UserReportForm, CustomUserChangeForm
from .models import Session_PC


def root(request):
    if request.user.is_authenticated:
        return redirect('/home')
    return render(request, 'root.html')


@login_required
def home(request):
    sessions = Session.objects.order_by('date')
    return render(request, 'home.html', {'sessions': sessions})


class HomeView(ListView):
    model=Session
    template_name = 'home.html'

    def dispatch(self, request, *args, **kwargs):
        sortType = None
        if kwargs.get('sortType'):
            sortType = kwargs['sortType']
        if sortType and hasattr(Session, sortType):
            self.queryset = Session.objects.order_by(sortType)
        else:
            self.queryset = Session.objects.order_by("date")
        return super(HomeView, self).dispatch(request, *args, **kwargs)


@login_required
def profile(request):
    user = request.user
    description = user.description
    return render(request, 'my_profile.html', {'user': user, 'description': description})


@login_required
def profile_edit(request):
    if request.method == 'POST':
        form = CustomUserChangeForm(request.POST, instance=request.user)
        if form.is_valid():
            prof = form.save(commit=False)
            prof.save()
            return redirect('profile')
    else:
        form = CustomUserChangeForm(instance=request.user)
    return render(request, 'edit_profile.html', {'form': form})


def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            user = form.save()
            current_site = get_current_site(request)
            mail_subject = 'Activate your account.'
            message = render_to_string('activate_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            to_email = form.cleaned_data.get('email')
            email = EmailMessage(
                mail_subject, message, to=[to_email]
            )
            email.send()
            return redirect('not_activated')
    else:
        form = UserRegistrationForm()
    return render(request, 'signup.html', {'form': form})


def not_activated(request):
    return render(request, 'not_activated.html')


def forgotpassword(request):
    return redirect('https://www.alzheimers.org.uk/find-support-near-you')


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = PC.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, PC.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.email_confirmed = True
        user.save()
        login(request, user)
        return redirect('home')
    else:
        return HttpResponse('Activation link is invalid!')


def session_creation(request):
    form = SessionForm()
    if request.method == "POST":
        form = SessionForm(request.POST)
        if form.is_valid():
            session = form.save(commit=False)
            session.save()
            return redirect('session_page', pk=session.pk)
        else:
            form = SessionForm()
    return render(request, 'session_creation.html', {'form': form})


def session_edit(request, pk):
    form = get_object_or_404(Session, pk=pk)
    if request.method == "POST":
        form = SessionForm(request.POST, instance=form)
        if form.is_valid():
            session = form.save(commit=False)
            session.save()
            return redirect('session_page', pk=session.pk)
    else:
        form = SessionForm(instance=form)
    return render(request, 'session_edit.html', {'form': form})


def session_page(request, pk):
    session = get_object_or_404(Session, pk=pk)
    attendees = session.attendees.all()
    namelist = ''
    for att in attendees:
        namelist += att.username + '\n'
    return render(request, 'session_page.html', {'session': session, 'attendees': namelist})


def report(request, reported_username):
    caller_user = request.user
    reported_user = get_object_or_404(PC, username=reported_username)

    last_report = Report.objects. \
        filter(caller_user_id=caller_user, reported_user_id=reported_user) \
        .filter(timestamp__gt=datetime.datetime.now() - datetime.timedelta(days=1)).first()

    if last_report is not None:
        # TODO ispravi kad budemo imali dobru profile/user hijerarhiju
        return redirect('home')

    instance = Report(caller_user_id=caller_user, reported_user_id=reported_user)

    if request.method == 'POST':
        form = UserReportForm(request.POST, instance=instance)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = UserReportForm()

    return render(request, 'report.html', {'form': form})


@login_required
def profile_test(request, username):
    user = get_object_or_404(PC, username=username)
    description = user.description
    return render(request, 'profile.html', {'user': user, 'description': description})


def apply_to_session(request, pk):
    form = ApplyToSessionForm()
    caller_user = request.user
    session = get_object_or_404(Session, Session_id=pk)
    instance = Session_PC(PC_id=caller_user, Session_id=session)
    if request.method == "POST":
        form = ApplyToSessionForm(request.POST, instance=instance)
        if form.is_valid():
            join = form.save(commit=False)
            join.save()
            return redirect('session_page', pk)
        else:
            form = ApplyToSessionForm()
    return render(request, 'session_join.html', {'form': form})