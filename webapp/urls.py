from django.contrib.auth import views as auth_views
from django.urls import path, re_path

from webapp.forms import CustomAuthForm
from . import views

urlpatterns = [
    path('', views.root, name='root'),
    path('home/', views.HomeView.as_view(), name='home'),
    path('home/<slug:sortType>', views.HomeView.as_view(), name='home'),
    path('profile/', views.profile, name='profile'),
    path('profile/edit', views.profile_edit, name='profile_edit'),
    path('profile/<slug:username>/', views.profile_test, name='profile_test'),
    path('login/', auth_views.LoginView.as_view(template_name='login.html', form_class=CustomAuthForm), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='logout.html'), name='logout'),
    path('register/', views.register, name='register'),
    path('not_activated/', views.not_activated, name="not_activated"),
    re_path(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            views.activate, name='activate'),
    path('forgotpassword', views.forgotpassword, name='forgotpassword'),
    path('session/new/', views.session_creation, name='session_creation'),
    path('session/<int:pk>', views.session_page, name='session_page'),
    path('session/<int:pk>/edit', views.session_edit, name='session_edit'),
    path('report/<slug:reported_username>/', views.report, name='report'),
    #  path('/<path:no-match>/', views.home, name='no-match'),
    path('session/<int:pk>/join', views.apply_to_session, name='apply_to_session'),
]
