package imp.platformTests.web;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;

import imp.platformTests.BrowserTests;
import imp.platformTests.helpers.Steps;

@RunWith(Parameterized.class)
public class RegisterTest {

	@Parameters(name = "BrowserDriver {0}")
	public static String[] params() {
		return BrowserTests.browsers;
	}

	WebDriver driver;
	Steps step;

	@Parameter
	public String browser;

	@Before
	public void before() {
		if      (browser == "chrome") 			driver = new ChromeDriver();
		else if (browser == "opera")  			driver = new OperaDriver();
		else if (browser == "gecko")  			driver = new FirefoxDriver();
		else if (browser == "edge")  			driver = new EdgeDriver();
		else if (browser == "internetexplorer")	driver = new InternetExplorerDriver();

		step = new Steps(driver);
	}

	@After
	public void after() {
		//step.quit();
	}

	@Test
	public void positiveTest() {
		step.goToRegister();
		step.fillRegisterForm("Username", "First", "Lastname", "first@lastname.com", "ThIs4600dpass", "ThIs4600dpass", true);
		step.sendRegForm();
	}
	
	@Test
	public void negativeTest() {
		step.goToRegister();
		step.fillRegisterForm("U!e", "fi", "Lname", "first@com", "Ts", "ThIs4dpass", false);
		step.sendRegForm();

	}
}
