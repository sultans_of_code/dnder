package imp.platformTests.web;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;

import imp.platformTests.BrowserTests;
import imp.platformTests.helpers.Steps;

@RunWith(Parameterized.class)
public class LoginTest {

	@Parameters(name = "BrowserDriver {0}")
	public static String[] params() {
		return BrowserTests.browsers;
	}

	WebDriver driver;
	Steps step;
	String user = "testtest", pass = "Sandi123";

	@Parameter
	public String browser;

	@Before
	public void before() {
		if      (browser == "chrome") 			driver = new ChromeDriver();
		else if (browser == "opera")  			driver = new OperaDriver();
		else if (browser == "gecko")  			driver = new FirefoxDriver();
		else if (browser == "edge")  			driver = new EdgeDriver();
		else if (browser == "internetexplorer")	driver = new InternetExplorerDriver();
		step = new Steps(driver);
		step.get(BrowserTests.root);
	}

	@After
	public void after() {
		step.quit();
	}

	@Test
	public void loginErrorPassword() {
		step.login(user, pass+"1");
		step.assertLoginError();	
	}

	@Test
	public void loginErrorUsername() {
		step.login(user+"a", pass);
		step.assertLoginError();	
	}

	@Test
	public void loginErrorBoth() throws InterruptedException {
		step.login(user+"a", pass+"1");
		step.assertLoginError();	
	}

	@Test
	public void loginAndLogout() {
		step.login(user, pass);
		step.pause(1);
		step.assertLoggedIn();
		step.logout();		
	}
}
