package imp.platformTests.web;

import org.junit.Assert;
import org.junit.Test;

import imp.platformTests.BrowserTests;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ResponseTest {
	@Test
	public void validate_response_code_200() {
		RequestSpecification httpRequest = RestAssured.given();
		for (String platform : BrowserTests.platformURLs) {
			Response response = httpRequest.get(platform);
			Assert.assertEquals(200, response.getStatusCode());
		}
	}
}
