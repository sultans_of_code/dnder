package imp.platformTests;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import io.github.bonigarcia.wdm.WebDriverManager;

@RunWith(Suite.class)
@SuiteClasses({
	imp.platformTests.web.LoginTest.class,
	imp.platformTests.web.ResponseTest.class,
	imp.platformTests.web.RegisterTest.class,
})
public class BrowserTests {

	public final static String[] browsers = {
//			"edge",
//			"internetexplorer",
			"chrome",
//			"gecko",
	};

	public final static String[] platformURLs = {
			"http://localhost:8000/",
//			"http://192.168.63.82:8088/",
	};

	public final static int platformSelect = 0;
	
	public final static String root = platformURLs[platformSelect];

	@BeforeClass
	public static void setup() {
		WebDriverManager.chromedriver().setup();
//		WebDriverManager.firefoxdriver().setup();
//		WebDriverManager.edgedriver().setup();
//		WebDriverManager.iedriver().setup();
	}
}
