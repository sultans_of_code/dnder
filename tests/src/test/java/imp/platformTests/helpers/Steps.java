package imp.platformTests.helpers;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import imp.platformTests.BrowserTests;

public class Steps {

	private WebDriver driver;
	private String user, pass;

	public String 
	signInButton = "/html/body/header/nav/div/div/div[2]/div/form[1]/button",
	username = "//*[@id=\"id_username\"]",
	password = "//*[@id=\"id_password\"]",
	logoutButton = "/html/body/header/nav/div/div/div[2]/div/form[2]/button",
	loginError = "/html/body/main/div/div/div/form/fieldset/div[1]/ul/li",
	userBar = "/html/body/main/div/div/h3",
	logInButton = "/html/body/main/div/div/div/form/div/button",
	register = "/html/body/header/nav/div/div/div[2]/div/form[2]/button",
	registerUsername = "//*[@id=\"id_username\"]",
	registerFN = "//*[@id=\"id_first_name\"]",
	registerLN = "//*[@id=\"id_last_name\"]",
	registerMail = "//*[@id=\"id_email\"]",
	regPass ="//*[@id=\"id_password1\"]",
	regConfirmPass = "//*[@id=\"id_password2\"]",
	regiDM = "//*[@id=\"id_is_dm\"]",
	sendRegister = "/html/body/main/div/div/div/form/div[8]/button";
	
	

	public void quit() {
		this.driver.quit();
	}

	public void get(String url) {
		driver.get(url);
	}

	public Steps(WebDriver driver) {
		super();
		this.driver = driver;
		this.user = "testtest";
		this.pass = "Sandi123";
		this.driver.get(BrowserTests.root);
	}

	public Steps(WebDriver driver, String user, String pass) {
		super();
		this.driver = driver;
		this.user = user;
		this.pass = pass;
		this.driver.get(BrowserTests.root);
	}

	public Steps(WebDriver driver, String user) {
		super();
		this.driver = driver;
		this.user = user;
		this.pass = "passpass";
		this.driver.get(BrowserTests.root);
	}

	public void login(String user, String pass) {
		pause(1);
		driver.findElement(By.xpath(signInButton)).click();
		pause(1);
		driver.findElement(By.xpath(username)).sendKeys(user);
		driver.findElement(By.xpath(password)).sendKeys(pass); 
		driver.findElement(By.xpath(logInButton)).click();
		pause(1);
	}

	public void login() {
		login(this.user, this.pass);
	}

	public void logout() {
		driver.findElement(By.xpath(logoutButton)).click();
	}
	
	public void goToRegister() {
		driver.findElement(By.xpath(register)).click();
	}
	
	public void fillRegisterForm(String username, String firstname, String lastname, String mail, String pass, String passconfirm, boolean dm) {
		driver.findElement(By.xpath(registerUsername)).sendKeys(username);
		driver.findElement(By.xpath(registerFN)).sendKeys(firstname);
		driver.findElement(By.xpath(registerLN)).sendKeys(lastname);
		driver.findElement(By.xpath(registerMail)).sendKeys(mail);
		driver.findElement(By.xpath(regPass)).sendKeys(pass);
		driver.findElement(By.xpath(regConfirmPass)).sendKeys(passconfirm);
		if (dm) {
			driver.findElement(By.xpath(regiDM)).click();
		}
	}
	
	public void sendRegForm() {
		driver.findElement(By.xpath(sendRegister)).click();
	}
	
	public void assertLoginError() {
		pause(1);
		Assert.assertEquals("Please enter a correct username and password. Note that both fields may be case-sensitive.", driver.findElement(By.xpath(loginError)).getText());
	}
	
	public void assertLoggedIn() {
		Assert.assertTrue(driver.findElement(By.xpath(userBar)).getText().contains("And, the day is almost here!")); 
	}	

	public void pause(float second) {
		try {
			Thread.sleep((long) (second*1000));
		} catch (Exception e) {
			return;		
		}		
	}

	public void pause() {
		pause(3);
	}
}
