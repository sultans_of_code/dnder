# DnDer

Traženje grupe za DnD session.

# Zadatak

Potrebno je razviti novu personaliziranu platformu za pronalaženje grupe za igru "Dungeons and Dragons(DnD)"
u obliku web/mobilne aplikacije. U aplikaciji će organizatori DnD seanse zadati tip igre, 
potreban broj igrača te odabrati otvorenost seanse. Zainteresirani igrači mogu vidjeti 
sve organizirane igre te mogu poslati zahtjev da dolaze na seansu. Ako je seansa otvorenog tipa,
igrač je automatski prihvaćen, dok ga kod zatvorenog tipa mora potvrditi organizator.

Za korištenje aplikacije potrebno je registrirati se. Nakon prijave u aplikaciju korisnik bira
hoće li biti organizator ili igrač. Organizatori za svaku seansu određuju kolika je cijena
za troškove(prostor, hrana i piće), a vlasnik platforme se naplaćuje kroz proviziju za pronalaženje
grupe. Predviđena metoda plaćanja gostovanja je PayPal.

Svi korisnici imaju svoje javne profile na kojima se nalaze osnovni podaci: korisničko ime,
slika(po izboru), kratak opis sebe(po izboru) te ocjene korisnika - kakav je kao domaćin/gost te 
kakav je kao igrač/DM(Dungeon master - voditelj igre). Također se na profilu prikazuju i sve
organizirane seanse korisnika unazad 2 godine. Za svaku seansu dostupni su detalji: naziv seanse,
lokacija, vrijeme početka, predviđeno trajanje, korisničko ime organizatora, korisnička imena igrača
koji su prijavljeni za seansu do sada, foto/video galerija druženja te recenzije igrača koji su
bili na seansi. Podatke o događanju kao i fotografije i video unose organizatori.
Na svakom profilu postoji opcija da se korisnika prijavi administratoru u slučaju prijevare
ili neorganiziranja seanse bez prethodne najave.

Igrači u aplikaciji mogu vidjeti popis grupa koje traže članove u odabranom vremenskom
razdoblju (24h, 7 dana, 30 dana) te poslati zahtjev za ulazak u grupu. Igrači se mogu i 
predomisliti te otkazati dolazak, međutim organizator im tada nije dužan vratiti cijeli
uplaćeni iznos. Prilikom traženja grupe igrač također može filtrirati grupe koje imaju
slobodno mjesto u ulozi DM-a. Organizatori mogu na svojim seansama biti i igrači. Broj
broj ljudi koji dolaze te njihova korisnička imena su javno vidljiva uz seansu na profilu
organizatora. Igrači mogu napisati recenziju te ocijeniti ostale igrače koji su dio grupe
za seanse koje su završile u proteklih 48h. Osim toga, igrači mogu odabrati postavke da
im aplikacija automatski šalje obavijesti o najnovijim seansama prema
zadanim kriterijima: tip igre, DM/igrač.

U sustavu postoje i administratori sustava koji upravljaju korisnicima, pregledavaju
prijavljene profile te u slučaju da je to potrebno zabranjuju pristup korisnicima koji
krše pravila.