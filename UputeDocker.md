# Guide for setting up development environment with docker

To setup docker development environment follow these steps:
1. Install [docker](https://docs.docker.com/v17.09/engine/installation/)
   and [docker-compose](https://docs.docker.com/compose/install/)
2. Open docker-compose.yml which is located in project root directory and edit line 7  
    ```
    - <path where to save local db>:/var/lib/postgresql/data
    ```  
    >Replace with path where you want to keep your local database   
     **Do not choose folder within project folder** 
    >- Windows users need to wrap both paths with "(double quote) and use `\\` double backslash instead of single backslash  
    >   - Example `- "C:\\postgres\\dnder:/var/lib/postgresql/data"`
3.  
    a) Start docker container from command line  
     - To start docker container from command line type 
         ```
         docker-compose up
         ```
     - To stop docker container type   
        ```
        docker-compose down
        ```
    b) Using Pycharm
     - In the upper right corner open "**Edit configurations**" -> "**docker-compose.yml:Compose Deployment**"   
     - To start/stop/restart docker and containers use "**Services**" tab in the bottom area