# DB setup

> ###Vazno!
>
> Ukoliko nemate ispravno podesen mount point gdje se sprema db ovaj postupak je potrebno ponoviti prilikom svakog pokretanja containera

1. Pokreniti docker-compose.yml
2. Otvoriti dropdown dok ne dodemo do containera `dnder_db_1`
3. Desni klik na `dnder_db_1 -> Exec`
4. Upisati `psql -h localhost -U postgres -d postgres` i stisnuti Enter
5. Otvorit ce se psql shell i od tamo trebamo stvoriti novog usera i bazu  
    ```postgresql
    CREATE USER django_admin WITH PASSWORD 'sultanofdjango' SUPERUSER;
    CREATE DATABASE dnder;
    ```
   > Ime i password ostavite kako pise u uputi jer je tako zapisano u settings.py
   >
6. Napraviti restart web containera `dnder_web_1`
7. Desni klik na `dnder_web_1 -> Exec` i upisati `python manage.py makemigrations` te stisnuti Enter
8. Desni klik na `dnder_web_1 -> Exec` i upisati `python manage.py migrate` te stisnuti Enter

Optional:
- Za kreiranje admin accounta (za aplikaciju) potrebno je otici na `dnder_web_1 -> Exec` i pokrenuti `python manage.py createsuperuser`   
  te slijediti daljnje promptove