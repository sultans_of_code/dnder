FROM python:3.7.5
RUN mkdir /dnder
WORKDIR /dnder
COPY requirements.txt /dnder/
RUN pip install -r requirements.txt
COPY . /dnder/
